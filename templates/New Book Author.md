---
tags:
  - author
---
  
```dataview
TABLE WITHOUT ID embed(link(cover, "100")) as Cover, link(file.link,title) as "Book", publishDate as Published
FROM "books"
WHERE contains(authors, [[]])
```