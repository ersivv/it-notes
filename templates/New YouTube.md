<%*
let url = await tp.system.clipboard();
let page = await tp.obsidian.request({url});
let p = new DOMParser();
let doc = p.parseFromString(page, "text/html");
let $ = (s) => doc.querySelector(s);

let canonical = $("link[rel='canonical']").href.split('&')[0];
let title = $("meta[name='title']").content;
file_name = title.replaceAll("/", "_").replaceAll(":"," ");

let description = $("meta[name='description']").content;
let keywords = $("meta[name='keywords']").content;
let shortlinkUrl = $("link[rel='shortlinkUrl']").href.split('?')[0];
//let imageSrc = $("link[rel='image_src']").href;
let videoId = shortlinkUrl.toString().replace("https://youtu.be/","");
let duration = $("meta[itemprop='duration']").content.slice(2, -1);
let authorUrl = $("span[itemprop='author'] > link[itemprop='url']").href;
let authorName = $("span[itemprop='author'] > link[itemprop='name']").getAttribute("content"); // Dot notation doesn't work here
//let thumbnailUrl = $("link[itemprop='thumbnailUrl']").href;
let datePublished = $("meta[itemprop='datePublished']").content;
file_name = file_name + ' ' + datePublished.slice(8,10) + '.' + datePublished.slice(5,7) + '.' + datePublished.slice(0,4);
//let uploadDate = $("meta[itemprop='uploadDate']").content;
let genre = $("meta[itemprop='genre']").content;

const timeStr = (time) => time.toString().padStart(2, '0');
let [minutes, seconds] = duration.split("M");
let hours = Math.floor(Number(minutes) / 60);
minutes = (Number(minutes) % 60);
duration = `${timeStr(minutes)}:${timeStr(seconds)}`;
if (hours > 0) {duration = `${timeStr(hours)}:` + duration}

-%>
---
title:  <% title %>
author:  <% authorName %>
channel: <% authorUrl %>
link:  <% shortlinkUrl %>
keywords:  [<% keywords %>]
genre: <% genre %> 
published: <% datePublished %>
duration: <% duration %> 
reviewed: <% tp.date.now() %>
tags: []

---


<center><iframe width="1120" height="630" src="https://www.youtube.com/embed/<% videoId %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

Описание:  

<% description %>

```timestamp-url 
 <% canonical %>
 ```

<% tp.file.rename(file_name) %>