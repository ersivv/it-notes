```dataview
Table WITHOUT ID embed(link(cover, "100")) as Cover, link(file.link,title) as "Book",
authors as Authors, publishDate as Published
From "books"
SORT publishDate DESC
```