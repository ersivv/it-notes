---
title: Запускаем Prometheus
subtitle: "Запускаем Prometheus / пер. с англ. А. Н. Киселева. – М.: Books.kz, 2023. – 392 с.: ил."
authors:
  - "[[Жюльен Пивотто]]"
  - "[[Брайан Бразил]]"
publisher: "[[Books.kz]]"
totalPage: 392
cover: "[[Запускаем Prometheus - Жюльен Пивотто, Брайан Бразил - 2023.png]]"
publishDate: 2023
isbn10: 
isbn13: 978-601-81034-1-4
tags:
  - k8s
  - prometheus
  - monitoring
---

Описание:

Возьмите на вооружение Prometheus – систему мониторинга на основе метрик, используемую тысячами организаций. Эта книга расскажет о системе мониторинга на основе метрик Prometheus и познакомит с наиболее важными ее аспектами, включая создание дашбордов и оповещений, прямое инструментирование кода, а также извлечение метрик из сторонних систем с помощью экспортеров. 
Авторы покажут, как использовать систему Prometheus для мониторинга приложений и инфраструктуры, как настроить Prometheus, Node Exporter и Alert manager, как использовать эти инструменты для мониторинга приложений и инфраструктуры.
Издание адресовано инженерам по надежности сайтов, администраторам Kubernetes и разработчикам программного обеспечения.

![[Запускаем Prometheus - Жюльен Пивотто, Брайан Бразил - 2023.pdf]]