---
title: Kubernetes Patterns
subtitle: 
authors:
  - "[[Bilgin Ibryam]]"
  - "[[Roland Huss]]"
publisher: "[[O’Reilly Media]]"
totalPage: "478"
cover: "[[Kubernetes Patterns - Bilgin Ibryam, Roland Huss - 2023.png]]"
publishDate: 2023
isbn10: 
isbn13: 978-1-098-13168-5
tags:
  - k8s
---

Описание:
The way developers design, build, and run software has changed significantly with the evolution of microservices and containers. These modern architectures offer new distributed primitives that require a different set of practices than many developers, tech leads, and architects are accustomed to. With this focused guide, Bilgin Ibryam and Roland Huss provide common reusable patterns and principles for designing and implementing cloud native applications on Kubernetes.

Each pattern includes a description of the problem and a Kubernetes-specific solution. All patterns are backed by and demonstrated with concrete code examples. This updated edition is ideal for developers and architects familiar with basic Kubernetes concepts who want to learn how to solve common cloud native challenges with proven design patterns.

You'll explore:

-   Foundational patterns covering core principles and practices for building and running container-based cloud native applications
-   Behavioral patterns that delve into finer-grained concepts for managing various types of container and platform interactions
-   Structural patterns for organizing containers within a Pod for addressing specific use cases
-   Configuration patterns that provide insight into how application configurations can be handled in Kubernetes
-   Security patterns for hardening the access to cloud native applications running on KubernetesAdvanced patterns covering more complex topics such as operators and autoscaling
- 
![[Kubernetes Patterns - Bilgin Ibryam, Roland Huss - 2023.pdf]]