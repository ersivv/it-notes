---
title: Запускаем Ansible
subtitle: "Запускаем Ansible. Простой способ автоматизации управления кон- фигурациями и развертыванием приложений. 3-е изд. / пер. с англ. А. Н. Киселева – М.: ДМК Пресс, 2023. – 482 с.: ил."
authors:
  - "[[Бас Мейер]]"
  - "[[Лорин Хохштейн]]"
  - "[[Рене Мозер]]"
publisher: "[[ДМК Пресс]]"
totalPage: "482"
cover: "[[Запускаем Ansible - Рене Мозер, Бас Мейер, Лорин Хохштейн - 2023.png]]"
publishDate: 2023
isbn10: 
isbn13: " 978-6-01763-867-2"
tags:
  - devops
  - ansible
---
Описание:
Среди множества имеющихся инструментов управления конфигурациями Ansible выделяется своими преимуществами, такими как небольшой объем, отсутствие необходимости устанавливать что-либо на управляемые хосты и простота в изучении и освоении. Наиболее существенное отличие этого издания от предыдущего – добавление шести новых глав, охватывающих применение контейнеров, фреймворка Molecule, платформы автоматизации Ansible Automation Platform и коллекций Ansible; приемы создания образов, поддержки об- лачной инфраструктуры и реализации конвейеров CI/CD. Книга предназначена разработчикам инструментов infrastructure as a code для автоматизации задач по подготовке и конфигурированию инфраструктуры.


![[Запускаем Ansible - Рене Мозер, Бас Мейер, Лорин Хохштейн - 2023.pdf]]