[https://bafista.ru/xpenology-zagruzchik-arc-i-patch-na-58-kamer/](https://bafista.ru/xpenology-zagruzchik-arc-i-patch-na-58-kamer/)

В этот раз я хотел бы вас познакомить с загрузчиком ARC для XPEnology, который может устанавливаться как вручную так и полностью автоматически, а так же имеет в своем составе патч на 58 камер.

![[attachments/6267329dad561936abe9bc78f16f32e7_MD5.jpg]]

-   [Сайт](https://auxxxilium.tech/redpill/) загрузчика ARC
-   Основная страница загрузчика ARC на [GitHub](https://github.com/AuxXxilium/AuxXxilium)
-   Скачать с [GitHub](https://github.com/AuxXxilium/arc/releases)
-   Загрузка ARC automated с [GitHub](https://github.com/AuxXxilium/arc-automated/releases) (больше не поддерживается автором, не качайте)
-   Wiki загрузчика ARC на [GitHub](https://github.com/AuxXxilium/AuxXxilium/wiki)

![[attachments/eb0b46c853c1661809309c99038b94d7_MD5.png]]

Доступные аддоны:

![[attachments/d448e6cfe5bfbbeb55240907faf31043_MD5.png]]

-   **acpid**: Демон ACPI (всегда выбран)
-   **разное**: Утилиты, которые нам нужны (всегда выбраны)
-   **cpuinfo**: Добавляет реальную информацию о процессоре в панель управления
-   **powersched**: Позволяет дискам перейти в спящий режим
-   **drivedbpatch**: Добавляет использованные диски в DSM DriveDB
-   **nvmeenable**: Разрешить пользователю создать NVMe StoragePool
-   **nvmeforce**: Добавляет диски NVMe в качестве хранилища (займет некоторое время (до 60 минут при первой загрузке из-за повторной синхронизации дисков, и для этого вам нужно рабочее хранилище данных в DSM)
-   **surveillance** – 58 лицензий для Surveillance Station 9.1.1-10728

После первой установки Surveillance Station с аддоном **surveillance** нужно перегрузить DSM.

![[attachments/a0db302bc356bd5cb7228acde8a0fa64_MD5.png]]

Если автоматический способ не подойдет, то нужно подключиться по ssh, перейти в режим супер пользователя root командой sudo -i , ввести еще раз пароль и затем такую команду:

```
/usr/bin/surveillance.sh
```

Эта команда в ручном режиме пропатчит Surveillance Station и у вас будет доступно 58 лицензий для камер.

У автора есть свой канал и [видео](https://www.youtube.com/watch?v=_ZZcd1_jh00) на английском языке.

Если у вас уже стоит XPEnology с этим загрузчиком, то обновите его, добавьте аддон и все будет то же самое. Если загрузчик другой, то у меня есть статья как установить данный аддон вручную в DSM
