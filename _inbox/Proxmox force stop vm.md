#proxmox 

Принудительно можно завершить процесс машины

Then we find the PID of the Machine process using the command.

```shell
ps aux | grep "/usr/bin/kvm -id VMID" | grep -v grep
```

Once we find the PID we kill the process using the command.

```shell
kill -9 PID
```