#zfs 

```shell
vim /etc/modprobe.d/zfs.conf
```

Добавить лимиты в байтах

```
# Setting up ZFS ARC size on Ubuntu as per our needs
# Set Max ARC size => 2GB == 2147483648 Bytes
options zfs zfs_arc_max=2147483648
 
# Set Min ARC size => 1GB == 1073741824
options zfs zfs_arc_min=1073741824
```

Обновить `initramfs`

```shell
sudo update-initramfs -u -k all
```

Перезагрузить

Проверить параметры

```shell
cat /sys/module/zfs/parameters/zfs_arc_min  
cat /sys/module/zfs/parameters/zfs_arc_max
```

Статистика

```shell
arcstat
```

Детализированный отчет

```shell
arc_summary
```
