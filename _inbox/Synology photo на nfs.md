#zfs #synology

```shell
zfs create zp/photo
```

```shell
zfs set compression=zstd zp/photo
```

```shell
zfs set sharenfs=rw,no_subtree_check,all_squash,anonuid=0,anongid=0 zp/photo
```


Степень сжатия

```shell
zfs get compression,compressratio zp/photo
```

В synology подключаем папку как nfs-шару

![[synology_photo_mount_nfs.png]]