#k8s #security #rbac

- [rbac-manager](https://github.com/FairwindsOps/rbac-manager) - оператор для удобной работы с RBAC. Позволяет биндить роли по лейблам на ресурсах, создавать биндинги в разных неймспейсах из одного манифеста и по-другому упрощает жизнь кластер админа

- [rbac-lookup](https://github.com/FairwindsOps/rbac-lookup) - CLI утилита для просмотра прав юзеров. Одной строкой выводит все биндинги
 - [audit2rbac](https://github.com/liggitt/audit2rbac)  - читает аудит логи к8с и генерирует RBAC исходя из того, что нужно юзерам
