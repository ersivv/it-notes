#zfs #nfs

Устанавливаем nfs-server

```shell
apt-get install -y nfs-kernel-server
```

```shell
zfs create zp/nfs-pvc
```

```shell
chmod -R 777 /zp/pvc
```

Отключаем время последнего доступа

```shell
zfs set atime=off zp/pvc
```

экспортируем

```shell
zfs set sharenfs=rw,no_subtree_check,no_root_squash zp/pvc
```

Список nfs экспортов 

```shell
showmount -e
```
