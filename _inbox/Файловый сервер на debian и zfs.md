#zfs 

Add or enable contrib repo to `/etc/apt/sources.list` using sed command

```shell
sed -r -i 's/^deb(.*)$/deb\1 contrib/g' /etc/apt/sources.list
```

Update apt repo database

```shell
apt update
```

Install zfs package on a Debian Linux 12

```shell
apt install linux-headers-amd64 zfsutils-linux zfs-dkms zfs-zed
```

Are you using a cloud server with cloud Linux kernel? Try:

```shell
apt install linux-headers-cloud-amd64 zfsutils-linux zfs-dkms zfs-zed
```

После перезагрузки создаем pool draid

```shell
zpool create zp draid /dev/sdb /dev/sdc /dev/sdd /dev/sde
```

Создаем разделы для лога и кеша

```shell
lvcreate -L 20G -n zlog fs-vg
lvcreate -L 80G -n zcache fs-vg
```

Листинг дисков

```shell
fdisk -l
```

Добавляем Log

```shell
zpool add zp log /dev/mapper/fs--vg-zlog
```

Добавляем Cache

```shell
zpool add zp cache /dev/mapper/fs--vg-zcache
```

Статус

```
# zpool status
  pool: zp
 state: ONLINE
config:

	NAME                                      STATE     READ WRITE CKSUM
	zp                                        ONLINE       0     0     0
	  draid1:2d:3c:0s-0                       ONLINE       0     0     0
	    scsi-0QEMU_QEMU_HARDDISK_drive-scsi2  ONLINE       0     0     0
	    scsi-0QEMU_QEMU_HARDDISK_drive-scsi3  ONLINE       0     0     0
	    scsi-0QEMU_QEMU_HARDDISK_drive-scsi1  ONLINE       0     0     0
	logs	
	  fs--test2--vg-zlog                      ONLINE       0     0     0
	cache
	  fs--test2--vg-zcache                    ONLINE       0     0     0
```


### Восстановление при отказе системного диска и переустановки ОС

Импорт пула с отсутствующим Log (флаг -m)

```shell
zpool import -f -m zp
```

Удаление Log

```shell
zpool remove zp /dev/mapper/fs--test--vg-zlog
```


### Синхронный режим

По умолчанию запись асинхронная - быстро но нет гарантии при сбои питания

Включение синхронной записи

```shell
zfs set sync=always mypool/dataset1
```


### Замена диска в pool

статус pool

```shell
zpool status
```

```
  pool: zp
 state: DEGRADED
status: One or more devices could not be used because the label is missing or
	invalid.  Sufficient replicas exist for the pool to continue
	functioning in a degraded state.
action: Replace the device using 'zpool replace'.
   see: https://openzfs.github.io/openzfs-docs/msg/ZFS-8000-4J
config:

	NAME                                      STATE     READ WRITE CKSUM
	zp                                        DEGRADED     0     0     0
	  draid1:2d:3c:0s-0                       DEGRADED     0     0     0
	    scsi-0QEMU_QEMU_HARDDISK_drive-scsi2  ONLINE       0     0     0
	    5559917750130180396                   UNAVAIL      0     0     0  was /dev/disk/by-id/scsi-0QEMU_QEMU_HARDDISK_drive-scsi3-part1
	    scsi-0QEMU_QEMU_HARDDISK_drive-scsi1  ONLINE       0     0     0
	logs	
	  fs--test2--vg-zlog                      ONLINE       0     0     0
	cache
	  fs--test2--vg-zcache                    ONLINE       0     0     0
```

список дисков

```shell
ls -ahlp /dev/disk/by-id/
```

```
drwxr-xr-x 2 root root 460 Oct 12 22:05 ./
drwxr-xr-x 9 root root 180 Oct 12 22:05 ../
lrwxrwxrwx 1 root root   9 Oct 12 22:05 ata-QEMU_DVD-ROM_QM00003 -> ../../sr0
lrwxrwxrwx 1 root root  10 Oct 12 22:05 dm-name-fs--test2--vg-root -> ../../dm-0
lrwxrwxrwx 1 root root  10 Oct 12 22:05 dm-name-fs--test2--vg-swap_1 -> ../../dm-1
lrwxrwxrwx 1 root root  10 Oct 12 22:05 dm-name-fs--test2--vg-zcache -> ../../dm-3
lrwxrwxrwx 1 root root  10 Oct 12 22:05 dm-name-fs--test2--vg-zlog -> ../../dm-2
lrwxrwxrwx 1 root root  10 Oct 12 22:05 dm-uuid-LVM-SfkxVOlDgI6MtVn5pUbhOToFNSzDL55g2ts42dJ7Gornw0qZtHkIi6uWsZ2a2hRp -> ../../dm-2
lrwxrwxrwx 1 root root  10 Oct 12 22:05 dm-uuid-LVM-SfkxVOlDgI6MtVn5pUbhOToFNSzDL55g7NSSAckY3KAzC0qOQaANz1mJwqLwK6Sl -> ../../dm-1
lrwxrwxrwx 1 root root  10 Oct 12 22:05 dm-uuid-LVM-SfkxVOlDgI6MtVn5pUbhOToFNSzDL55gnSBQpuPLzXH9yHh7YOphLwOg5SrZPTOi -> ../../dm-0
lrwxrwxrwx 1 root root  10 Oct 12 22:05 dm-uuid-LVM-SfkxVOlDgI6MtVn5pUbhOToFNSzDL55gWkOSg6CTKXqexXk31M7XkfWNmI4veUi6 -> ../../dm-3
lrwxrwxrwx 1 root root  10 Oct 12 22:05 lvm-pv-uuid-XWMlcN-uQ9G-2FQs-w1Le-WlKa-4RAr-hb9Bbe -> ../../sda5
lrwxrwxrwx 1 root root   9 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi0 -> ../../sda
lrwxrwxrwx 1 root root  10 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-part1 -> ../../sda1
lrwxrwxrwx 1 root root  10 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-part2 -> ../../sda2
lrwxrwxrwx 1 root root  10 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-part5 -> ../../sda5
lrwxrwxrwx 1 root root   9 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi1 -> ../../sdb
lrwxrwxrwx 1 root root  10 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi1-part1 -> ../../sdb1
lrwxrwxrwx 1 root root  10 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi1-part9 -> ../../sdb9
lrwxrwxrwx 1 root root   9 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi2 -> ../../sdc
lrwxrwxrwx 1 root root  10 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi2-part1 -> ../../sdc1
lrwxrwxrwx 1 root root  10 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi2-part9 -> ../../sdc9
lrwxrwxrwx 1 root root   9 Oct 12 22:05 scsi-0QEMU_QEMU_HARDDISK_drive-scsi3 -> ../../sdd

```

```shell
lsblk
```

```
NAME                     MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda                        8:0    0   15G  0 disk 
├─sda1                     8:1    0  487M  0 part /boot
├─sda2                     8:2    0    1K  0 part 
└─sda5                     8:5    0 14.5G  0 part 
  ├─fs--test2--vg-root   254:0    0  8.4G  0 lvm  /
  ├─fs--test2--vg-swap_1 254:1    0  976M  0 lvm  [SWAP]
  ├─fs--test2--vg-zlog   254:2    0    1G  0 lvm  
  └─fs--test2--vg-zcache 254:3    0    1G  0 lvm  
sdb                        8:16   0    5G  0 disk 
├─sdb1                     8:17   0    5G  0 part 
└─sdb9                     8:25   0    8M  0 part 
sdc                        8:32   0    5G  0 disk 
├─sdc1                     8:33   0    5G  0 part 
└─sdc9                     8:41   0    8M  0 part 
sdd                        8:48   0    5G  0 disk 
sr0                       11:0    1 1024M  0 rom  
```

Заменяем в pool диск

```shell
zpool replace -f zp 5559917750130180396 /dev/sdd
```