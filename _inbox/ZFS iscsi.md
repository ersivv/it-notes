#zfs #iscsi

Создаем fs

```shell
zfs create -V 50g zp/iscsi-pve
```


Делаем доступным

```shell
zfs set shareiscsi=on zp/iscsi-pve
```

Список таргетов

```shell
scsitadm list target
```

![[Pasted image 20231019081036.png]]