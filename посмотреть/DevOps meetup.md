---
title:  DevOps meetup
author:  Evrone Development
channel: http://www.youtube.com/@EvroneDevelopment
link:  https://youtu.be/k4j2m-QfALU
keywords:  [video, sharing, camera phone, video phone, free, upload]
genre: Entertainment 
published: 2023-10-25T23:01:18-07:00
duration: 01:53:47 
reviewed: 2023-10-26
tags: []

---


<center><iframe width="1120" height="630" src="https://www.youtube.com/embed/k4j2m-QfALU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

Описание:  

[4:08](https://www.youtube.com/watch?v=k4j2m-QfALU&t=248s) - Переходим с Ingress на Gateway API — Александр Кириллов, Evrone
[20:41](https://www.youtube.com/watch?v=k4j2m-QfALU&t=1241s) - Сессия Q&A 
[37:53](https://www.youtube.com/watch?v=k4j2m-QfALU&t=2273s) - "Serverless" как инструмент для разработчиков без опыта в DevOps — Нуралем Абизов, Re:start Financial 
[55:35](https://www.youtube.com/watch?v=k4j2m-QfALU&t=3335s) - Сессия Q&A 
[1:08:11](https://www.youtube.com/watch?v=k4j2m-QfALU&t=4091s)\- Переезд в облако из корпоративной инфраструктуры — Владимир Пашковский, Magnit IT-Lab Innopolis 
[1:38:30](https://www.youtube.com/watch?v=k4j2m-QfALU&t=5910s) - Сессия Q&A

```timestamp-url 
 https://www.youtube.com/watch?v=k4j2m-QfALU
 ```

