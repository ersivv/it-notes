---
title:  Вебинар для CTO и DevOps «Автоматизация создания окружений для R&D-команд»
author:  Hilbert Team
channel: http://www.youtube.com/@hilbertteam
link:  https://youtu.be/jTx0sQ5pcY0
keywords:  [video, sharing, camera phone, video phone, free, upload]
genre: Education 
published: 2023-10-13T03:22:31-07:00
duration: 01:22:04 
reviewed: 2023-10-27
tags: []

---


<center><iframe width="1120" height="630" src="https://www.youtube.com/embed/jTx0sQ5pcY0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

Описание:  

Чтобы сделать процесс разработки цифровых продуктов максимально быстрым и эффективным, важно избавить своих разработчиков от рутинной настройки окружений и помочь им сосредоточиться на главном – написании кода. 

На вебинаре мы расскажем расскажем, как **облегчить жизнь вашим разработчикам** и внедрить подходы по автоматизации создания R&D-окружений, а также покажем, **как вы можете организовать это на практике** в своей организации. 

_Программа:_  
1. Создание динамических окружений в Kubernetes (пример реализации; плюсы и минусы),    
2. Создание статических окружений с помощью Terraform и Terragrunt  (пример реализации; плюсы и минусы),    
3. Создание окружений с помощью CrossPlane и ArgoCD (пример реализации; плюсы и минусы; планы Yandex Cloud на развитие CrossPlane),  
4. Как выбрать подходящий сценарий в зависимости от требований бизнеса.

_Спикеры:_   
**Михаил Кажемский** – ведущий DevOps-инженер Hilbert Team.  
**Антон Брехов** – партнерский архитектор Yandex Cloud.

_Организаторы:_   
**Hilbert Team** – отраслевой эксперт в DevOps, DevSecOps, DataOps, сертифицированный партнер Yandex Cloud со специализациями по DevOps и Data Platform.  
**Yandex Cloud** — универсальная облачная платформа, предоставляющая крупным корпорациям, среднему и малому бизнесу, а также индивидуальным разработчикам масштабируемую инфраструктуру, сервисы хранения данных, инструменты машинного обучения и средства разработки.

```timestamp-url 
 https://www.youtube.com/watch?v=jTx0sQ5pcY0
 ```

