---
title: Владение и заимствование // Демо-занятие курса «Rust Developer. Professional»
author: OTUS Онлайн - образование
channel: http://www.youtube.com/@otus_education
link: https://youtu.be/Eb5AdzctpQI
keywords:
  - video
  - sharing
  - camera phone
  - video phone
  - free
  - upload
genre: Education
published: 2023-10-23T23:27:55-07:00
duration: 01:21:12
reviewed: 2023-10-27
tags:
  - rust
---


<center><iframe width="1120" height="630" src="https://www.youtube.com/embed/Eb5AdzctpQI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

Описание:  

На вебинаре мы увидим, как несколько дополнительных ограничений позволяют компилятору Rust обходится без сборки мусора и гарантировать корректное использован...

```timestamp-url 
 https://www.youtube.com/watch?v=Eb5AdzctpQI
 ```

