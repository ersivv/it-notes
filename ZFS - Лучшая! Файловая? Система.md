---
title: ZFS - Лучшая! Файловая? Система
author: Slava Ra
channel: http://www.youtube.com/@RaSlaRu
link: https://youtu.be/y-_MO6QSvZk
keywords:
  - zfs
  - linux
genre: Education
published: 2023-05-27
duration: 01:05:48
reviewed: 2023-09-22
tags:
  - zfs
---


<center><iframe width="1120" height="630" src="https://www.youtube.com/embed/y-_MO6QSvZk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

Описание:  
2023-05-25 - провёл доклад для группы LUG_EKB. 
Годом ранее, этот доклад был сделан мною на внутреннем митапе Тинькофф 
Презентация: [[Presentation ZFS - Лучшая! Файловая? Система.pdf]]
Временные метки: 
[00:00](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=0s) - Вступление 
[01:13](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=73s) - План доклада 
[01:54](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=114s) - 1. Описание и состав ZFS: История появления и развития ZFS 
[04:02](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=242s) - Cостав ZFS 
[06:23](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=383s) - Команды для управления ZFS 
[08:58](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=538s) - Создание ZFS-пула 
[11:20](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=680s) - Компоненты ZFS 
[12:48](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=768s) - Контроль состояния ZFS 
[14:48](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=888s) - 2. Ключевые возможности и плюсы ZFS: Copy-on-Write 
[15:50](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=950s) - Snapshots 
[17:48](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=1068s) - Передача Snapshot'ов по SSH 
[19:22](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=1162s) - Возможности конфигурирования DataSet/Pool 
[23:02](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=1382s) - Сжатие данных 
[28:20](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=1700s) - 3. ZFS vs ... 
[40:19](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=2419s) - 4. Правильные случаи применения ZFS 
[44:16](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=2656s) - 5. Минусы ZFS 
[48:32](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=2912s) - ВЫВОДЫ 
[49:45](https://www.youtube.com/watch?v=y-_MO6QSvZk&t=2985s) - Обсуждение доклада: секция вопросов и ответов

##
```timestamp-url 
 https://www.youtube.com/watch?v=y-_MO6QSvZk
 ```

